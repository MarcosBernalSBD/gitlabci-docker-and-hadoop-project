# Overview

Small project to launch gitlab ci using docker, hadoop and spark. 

To run it locally, execute:
  - `gitlab-runner exec docker --docker-privileged hadoop_and_spark_build_job`
  - Or in case you want to specify any timeout
    - `gitlab-runner exec docker --docker-wait-for-services-timeout 20 --timeout 3600 --docker-privileged hadoop_and_spark_build_job`

To install `gitlab-runner` on linux, follow the following [link of gitlab documentation](https://docs.gitlab.com/runner/install/linux-manually.html).
In my case, they were:
- `sudo wget -O /usr/local/bin/gitlab-runner https://gitlab-runner-downloads.s3.amazonaws.com/latest/binaries/gitlab-runner-linux-amd64`
- `sudo chmod +x /usr/local/bin/gitlab-runner`

## Files

- *.gitlab-ci.yml*: contains the jobs to be executed by the CI component
- *docker-compose.yml*: contains the environment with the images and configuration
- *hadoop.env*: contains the hadoop configuration 

## Environment

There are 4 types of services in the docker-compose:
- hadoop namenode
- hadoop datanode
- spark-master
- spark-worker

All these images are taken from the official [big data europe repository](https://github.com/big-data-europe/docker-hadoop-spark-workbench).
